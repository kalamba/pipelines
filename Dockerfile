FROM mhart/alpine-node:10.6.0
LABEL version="1.0.0"
LABEL maintainer="Michal Ochman <michal@kalambagames.com>"

ENV NODE_PATH=/usr/lib/node_modules \
    PHRASEAPP_VERSION=1.7.4

RUN apk update &&\
    # install build dependencies
    apk add --no-cache --virtual runtime-dependencies \
      git \
      cairo \
      jpeg \
      pango \
      &&\
    apk add --no-cache --virtual build-dependencies \
      build-base \
      cairo-dev \
      jpeg-dev \
      pango-dev \
      curl \
      &&\
    # install phraseapp cli bin
    mkdir -p /usr/local/share/phraseapp && cd /usr/local/share/phraseapp &&\
    curl -L https://github.com/phrase/phraseapp-client/releases/download/${PHRASEAPP_VERSION}/phraseapp_linux_386.tar.gz | tar zxv &&\
    ln -s /usr/local/share/phraseapp/phraseapp_linux_386 /usr/local/bin/phraseapp &&\
    # allows npm to install globally (-g)
    npm config set user 0 &&\
    npm config set unsafe-perm true &&\
    # install canvas for jsdom
    npm install -g canvas &&\
    # delete build dependencies
    apk del build-dependencies &&\
    rm /var/cache/apk/*

